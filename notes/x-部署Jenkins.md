
-- 部署Jenkins
https://www.jianshu.com/p/cfdf0d90185e 安装Jenkins
https://www.jianshu.com/p/8b47631ae374 (含有通过端口杀进程)
https://www.cnblogs.com/fakerblog/p/8482682.html
```
cd /g-software/
mkdir jenkins
cd jenkins
// wget http://ftp-nyc.osuosl.org/pub/jenkins/war-stable/2.235.1/jenkins.war 下载慢（需要穿墙）
scp ./jenkins.war root@m.jiangjiesheng.cn:/g-software/jenkins
netstat -ntlpa | grep 8083 确定未使用的端口
nohup java -Dhudson.util.ProcessTree.disable=true -jar jenkins.war --logfile=server.log --httpPort=8083 > server.log 2>& 1 &


/Users/jiangjiesheng/.jenkins/hudson.model.UpdateCenter.xml (https://www.cnblogs.com/yjssjm/p/12658970.html)


账号密码在日志中
admin/d6e4def25a9d4b66a905626d047c71a6
    或者 /root/.jenkins/secrets/initialAdminPassword

密码：
jiangjiesheng用户名/密码:admin,密码见云笔记《12-网站维护》

查看已使用的端口 
netstat -ntlp

安装目录可能在 /root/.jenkins
 

http://jenkins.jiangjiesheng.cn/pluginManager/advanced
https://updates.jenkins.io/update-center.json改为http://mirror.xmission.com/jenkins/updates/update-center.json
https://blog.csdn.net/nioutian/article/details/103337085

http://jenkins.jiangjiesheng.cn/pluginManager/available 缺失的还是要在这里在线安装就可以了。


http://jenkins.jiangjiesheng.cn/exit 关闭
http://jenkins.jiangjiesheng.cn/restart
http://jenkins.jiangjiesheng.cn0/reload

1.
安装"Locale"插件
系统管理 -> 系统设置 -> Locale &&  Localization: Chinese (Simplified) -> zh_cn

2.
安装好"Maven Integration" plugin，在“新建任务”里就可以看到“构建一个maven项目”选项。

3.gitlab (会自动安装 git client ，git ， gitlab)



echo ${JAVA_HOME}
 /g-software/maven/apache-maven-3.0.5/

不需要再设置Credentials


3.
https://www.jianshu.com/p/8b47631ae374

clean install -DskipTests

https://www.baidu.com/s?wd=jenkins%20%E5%85%B3%E9%97%AD%E7%89%88%E6%9C%AC%E6%A3%80%E6%9F%A5%E6%9B%B4%E6%96%B0&rsv_spt=1&rsv_iqid=0x91087781000072b1&issp=1&f=8&rsv_bp=1&rsv_idx=2&ie=utf-8&rqlang=cn&tn=baiduhome_pg&rsv_enter=1&rsv_dl=tb&rsv_btype=t&inputT=3998&rsv_t=b5cdqANCiMatErji8Z1VpeXHb%2FBsp3P8o6ys%2BkaJM0eNzq3niyI32ee4g9p7aR60t86a&oq=jenkins%2520%25E5%2585%25B3%25E9%2597%25ADjenkins%25E6%25A3%2580%25E6%259F%25A5%25E6%259B%25B4%25E6%2596%25B0&rsv_pq=fc82b6ff0005dc1d&sug=jenkins&rsv_sug3=180&rsv_sug2=0&rsv_sug4=9707

```
```
#!/bin/bash

#要以 #! 开头并放在第一行，关于是否需要打印执行脚本的设置，见本脚本末尾说明


#赋值好像都不要有空格

function killproject()
{
#由于我这边多个项目jar包名一致了，所以并没有用 ps -ef | grep jar包名 来查进程id，而是通过端口号来查的
  project_pid=$(netstat -lnp | grep $port|awk '{print $7}'|cut -d/ -f1)
  if [  $project_pid > 0 ];then
        echo "项目已经启动了，开始关闭项目，项目pid为: $project_pid "
        kill -9 $(netstat -lnp | grep $port|awk '{print $7}'|cut -d/ -f1)
        echo "项目关闭成功，开始重启项目。。。"
  else
        echo "项目未启动，直接启动"
  fi
}
function start_project()
{
        echo "删除旧jar包（以后最好备份）"
        echo "参考 /j-deploy/mmall-backend-deploy.sh"
        rm /j-deployed/jenkins-jar/es-dianping-app/$jarPackageName.jar
        
        echo "从jenkins中移动jar包"
        mv /root/.jenkins/workspace/$jenkinsTaskName/target/$jarPackageName.jar /j-deployed/jenkins-jar/es-dianping-app/
        echo " "
        
        echo "###################开始删除编译的jar包等文件###################"
        echo ""
        echo "删除jenkins中jar包，见学习笔记 关键词 linux查找符合条件的文件并删除"

		#find 路径以/结尾， name参数实际需要单引号，这里使用变量，不使用任何引号，可以workspace开头的“sh -l”查看实际执行脚本
        find /root/.jenkins/workspace/$jenkinsTaskName/ -name $jarPackageName* |xargs rm -rf 
        find /root/.jenkins/jobs/$jenkinsTaskName/modules/cn.jiangjiesheng\$dianping/builds/ -name $jarPackageName* |xargs rm -rf
        find /root/.m2/repository/cn/jiangjiesheng/dianping/ -name $jarPackageName* |xargs rm -rf
          
        echo "后续使用 find / -name 'dianping-0.0.1-SNAPSHOT.jar'查询，注意是单引号，是否只有/j-deployed/jenkins-jar/有jar包"
        echo ""
        echo "###################完成删除编译的jar包等文件###################"
        echo " "
        
        source /etc/profile
        echo "正在启动项目。。。"
        cd /j-deployed/jenkins-jar/es-dianping-app/
        #这里不是使用追加到日志的，应该可以改 >> 未测试
        #nohup java -jar dianping-0.0.1-SNAPSHOT.jar > warpper.log &2>1 &
        nohup java -jar $jarPackageName.jar > warpper.log &2>1 &

}

function check_project()
{
i=1
issuccess=0

while [ $i -le $sumSecond ]
do
  check_pid=$(netstat -lnp | grep $port|awk '{print $7}'|cut -d/ -f1)
  if [ $check_pid  > 0 ];then
        echo "项目启动成功，pid = $check_pid  "
        let issuccess=1
        break
  else
        sleep 1s
        echo "启动中..."$i"s/$sumSecond""s"
        let i++
  fi
done

#如果使用==，那等号两边应该不能有空格，未测试
if [ $issuccess -eq 1 ];then
        #echo "启动成功"
        exit 0
else
        echo "启动失败"
        set -e #注意，这句最重要，一定要先设置这个，非常感谢 诺亚舟 的提示
        exit 1  #然后再退出，jenkins就会报红显示构建失败
fi
}

#检测项目启动等待的总时长，单位秒
sumSecond=120
#检测启动端口
port=8010
#jenkins任务名称
jenkinsTaskName=es-dianping-APP
#jar包名称
jarPackageName=dianping-0.0.1-SNAPSHOT


killproject
start_project
check_project


#说明：
#以#!开头,此标识必须放在第一行
# #!/binxxx/sh -l    打印当前正在执行的脚本

# #!/bin/bash 不打印当前正在执行的脚本
```

find / -name  'dianping-0.0.1-SNAPSHOT.jar'：

root/.m2/repository/cn/jiangjiesheng/dianping/0.0.1-SNAPSHOT/dianping-0.0.1-SNAPSHOT.jar
/root/.jenkins/jobs/es-dianping-APP/modules/cn.jiangjiesheng$dianping/builds/1/archive/cn.jiangjiesheng/dianping/0.0.1-SNAPSHOT/dianping-0.0.1-SNAPSHOT.jar
/root/.jenkins/workspace/es-dianping-APP/target/dianping-0.0.1-SNAPSHOT.jar


继续 https://www.jianshu.com/p/8b47631ae374 构建后操作中选择Send build artifacts Over SSH， 这部废弃
使用 配置构建成功后的动作，添加shell

-- http://jenkins.jiangjiesheng.cn/configure

[JENKINS] Archiving /root/.jenkins/workspace/es-dianping-APP/pom.xml to cn.jiangjiesheng/dianping/0.0.1-SNAPSHOT/dianping-0.0.1-SNAPSHOT.pom
[JENKINS] Archiving /root/.jenkins/workspace/es-dianping-APP/target/dianping-0.0.1-SNAPSHOT.jar to cn.jiangjiesheng/dianping/0.0.1-SNAPSHOT/dianping-0.0.1-SNAPSHOT.jar
 
 邮箱验证测试 java.net.SocketException: Connection closed by remote host
 https://www.colabug.com/2020/0410/7235672/
  系统管理员邮件地址需要和邮件通知->用户名一致
 
    有用：springboot 关于 Class path contains multiple SLF4J bindings.警告的解决
    排除依赖的方法（双击一下） https://blog.csdn.net/wohaqiyi/article/details/81009689
    https://blog.csdn.net/zhangdongan1991/article/details/96115397
    
    启动失败的原因应该时间太短，可能也是跟衍生进程有关系。
    
    检测是否启动 （可以看上面的脚本了）
    https://blog.csdn.net/c5113620/article/details/79169120?utm_source=blogxgwz2 结合一下，判断端口
    
    后续再研究日志最后 Process leaked file descriptors. See https://jenkins.io/redirect/troubleshooting/process-leaked-file-descriptors for more information
    
   
     
    
     
    