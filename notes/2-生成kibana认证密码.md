kibana 本身没有用户名密码的设置，可以使用 nginx 来实现。

步骤
（1）生成密码文件
// 安装工具包
yum install httpd-tools;
// 生成密码，用户名 admin
htpasswd -c /usr/local/nginx/conf/custom-password/htpasswd-kibana-dianping admin

# 提示输入2遍密码
New password:
Re-type new password:
Adding password for user admin
（2）nginx 配置
location / {
    # 设置 auth
    auth_basic "大众点评项目kibana登录认证";
    auth_basic_user_file /usr/local/nginx/conf/custom-password/htpasswd-kibana-dianping;

    # 转发到 kibana
    proxy_pass http://192.168.3.132:5601;
    proxy_redirect off;
}
重新加载：

nginx -s reload
（3）测试
访问 nginx 地址，就会弹出认证窗口：

参考：https://www.jianshu.com/p/fbc3a80c05c5